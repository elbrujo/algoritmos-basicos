/*
Ejemplo muy simple y básico de una función recursiva
El programa llena un array de 10 elementos con 10 número ramdom
para finalmente sumarlos y mostrar en pantalla el resultado.
*/

#include <iostream>
#include <stdlib.h>
#include <stdio.h>

using namespace std;

#define ARRAY 10

//llenar el array con valores ramdom
void valores(char *array) {
	for (int index = 0; index < ARRAY; index++) {
		array[index] = rand() % ARRAY;
	}
}

//solo imprimir el array
void imprime(char *array) {
	for (int index = 0; index < ARRAY; index++) {
		printf("[%d] = %d\n", index, array[index]);
	}
}

/* la función recursiva, se llama a si misma para sumar
la totalidad de los elementos en el array */
int suma(char *array, int size) {
	if (size == 1) {
		return array[size - 1];
	} else {
		return suma(array, size - 1) + array[size - 1];
	}
}

//main
int main () {
	char array[ARRAY];
	valores(array);
	imprime(array);
	printf("La suma es: %d\n", suma(array, ARRAY));
	return 0;
}
