/*
Ejemplo muy simple y básico del algoritmo de busqueda binaria
El programa llena un array de 1000000 elementos con 1000000 números ramdom,
recibe como parámetro de programa el número a buscar y
muestra en pantalla el resultado de la búsqueda
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>

#define ARRAY 1000000

/* llenar el arreglo */
void valores(int *array) {
	for (int index = 0; index < ARRAY; index++) {
		array[index] = index + 1;
	}
}

/* salida a pantalla */
void imprime(int *array) {
	for (int index = 0; index < ARRAY; index++) {
		printf("[%d] = %d\n", index, array[index]);
	}
}

/* función principal del algoritmo de búsqueda binaria, implícitamente realizar recursividad */
void searchBinary(int *array, int data, int inferior, int superior) {

	int middle = ((inferior + superior) / 2);

	printf("inferior: %d, superior: %d\n", inferior, superior);

	if (array[middle] == data) {
		printf("Número %d encontrado!!!\n", data);
	} else if ((superior - inferior) <= 1) {
		printf("Número %d NO encontrado!!!\n", data);
	} else if (data < array[middle]) {
		searchBinary(array, data, inferior, middle);
	} else {
		searchBinary(array, data, middle + 1, superior);
	}
}

//main
int main(int narg, char **args) {

	if (narg != 2) {
		printf("%s\n", "Error: Falta número a buscar, ejemplo:");
		printf("%s\n", "./<ejecutable> <numero_buscar>");
		return -1;
	}

	int array[ARRAY];
	valores(array);
	//imprime(array);
	searchBinary(array, atoi(args[1]), 0, ARRAY - 1);
	return 0;
}
