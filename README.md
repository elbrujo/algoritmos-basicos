# Algoritmos-Basicos

Ejemplos simples y básicos de típicos algoritmos de programación en C++.

- Rescursividad
- Búsqueda Binaria
- Ordenamiento de Burbuja
- (Próximamamente más en otros lenguajes)

Forma de compilar (linux)

g++ -o <archivo_exe> <archivo_fuente>