/*
Ejemplo muy simple y básico del algoritmo de ordenamiento por burbuja
El programa llena un array de 50 elementos con 50 números ramdom,
se muestra en pantalla antes y después del ordenamiento
*/

#include <stdlib.h>
#include <iostream>

using namespace std;

#define ARRAY 50

/* MOSTRAR EL ARREGLO PANTALLA */
void showArreglo(int *array) {
	for (int index = 0; index < ARRAY; index++) {
		cout << "[" << index << "] = " << array[index] << endl;
	}
}

/* FUNCION PRINCIPAL DE ORDENAMIENTO POR BURBUJA */
void ordenar(int *array) {

	int aux = 0;

	for (int pivote = 0; pivote < ARRAY - 1; pivote++) {
		for (int index = pivote + 1; index < ARRAY; index++) {
			if (array[pivote] > array[index]) {
				aux = array[pivote];
				array[pivote] = array[index];
				array[index] = aux;
			}
		}
	}
}

//main
int main() {
	int arreglo[ARRAY];

	//cargar el arreglo
	for (int index = 0; index < ARRAY; index++) {
		arreglo[index] = rand() % ARRAY;
	}

	cout << "Ante del ordenamiento\n";
	showArreglo(arreglo);
	ordenar(arreglo);
	cout << "Después del ordenamiento\n";
	showArreglo(arreglo);
	return 0;
}
